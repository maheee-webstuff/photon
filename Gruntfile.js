const sass = require('node-sass');

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: ['dist/*'],
    sass: {
      options: {
        implementation: sass,
        sourceMap: true
      },
      dist: {
        files: {
          'dist/style/photon.css': 'sass/photon.scss',
          'dist/style/fontawesome.css': 'sass/fontawesome.scss'
        }
      }
    },
    copy: {
      main: {
        files: [
          {
            expand: true,
            src: ['./node_modules/@fortawesome/fontawesome-free/webfonts/*'],
            dest: 'dist/webfonts/',
            flatten: true,
            filter: 'isFile'
          }
        ],
      },
    },
    htmlbuild: {
      dist: {
        src: 'doc/index.html',
        dest: 'dist/',
        options: {
          relative: true,
          styles: {
            photon: [
              'dist/style/photon.css',
              'dist/style/fontawesome.css'
            ],
            doc: [
              'doc/style/style.css'
            ]
          },
          scripts: {
            bundle: [
              'doc/scripts/vendor/jquery-3.3.1.slim.min.js',
              'doc/scripts/nav.js',
              'doc/scripts/source.js'
            ]
          },
          sections: {
              templates: 'doc/templates/**/*.html',
              components: 'doc/components/**/*.html'
          },
          data: {
            // Data to pass to templates
            version: "0.1.0",
            title: "test",
          }
        }
      }
    },
    watch: {
      css: {
        files: ['sass/**/*.scss'],
        tasks: ['sass'],
        options: {
          livereload: 35729
        }
      },
      html: {
        files: ['doc/**/*.html'],
        tasks: ['htmlbuild'],
        options: {
          livereload: 35729
        }
      }
    },
    connect: {
      server: {
        options: {
          port: 9000,
          base: {
            path: './dist/',
            options: {
              index: 'index.html',
            }
          }
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-html-build');
  grunt.loadNpmTasks('grunt-sass');

  grunt.registerTask('build', ['clean', 'copy', 'sass', 'htmlbuild']);
  grunt.registerTask('serve', ['build', 'connect', 'watch']);
  grunt.registerTask('default', ['serve']);

};
