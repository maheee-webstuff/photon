# Photon Elements

Photon Elements is a small CSS framework for creating web applications.

It was initially a fork of [Photon](http://photonkit.com/) ([Github](https://github.com/connors/photon)) and is still heavily based upon Photon.

![](doc/screenshot.png)

## Documentation

A full list of all components can be found [here](https://maheee-webstuff.gitlab.io/photon/).


## Building/Running

### Build the project

* `npm install`
* `npm run build`

### Run the project

* `npm install`
* `npm run serve`
