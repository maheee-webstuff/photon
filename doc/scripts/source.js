$(() => {

  const sourceView$ = $('#sourceView');
  const sourceViewContent$ = $('#sourceView > article');
  const sourceViewClose$ = $('#sourceViewClose');

  sourceView$.hide();
  sourceViewClose$.click(() => {
    sourceView$.hide();
  });

  $('#content > section > article > section').each((i, element) => {
    const source = element.outerHTML.replace(/[&]/g, '&amp;').replace(/[>]/g, '&gt;').replace(/[<]/g, '&lt;');
    const sourceLink = $('<a href="#" title="Show Source"><i class="fas fa-code"></i></a>');

    $(element).parent().find('> header > h2').append($('<span>&nbsp;</span>')).append(sourceLink);

    sourceLink.click((event) => {
      sourceView$.hide();
      sourceViewContent$.empty();
      sourceViewContent$.append($('<pre>' + source + '</pre>'));
      sourceView$.show();
      sourceViewContent$.focus();
      event.preventDefault();
    });
  });

});
