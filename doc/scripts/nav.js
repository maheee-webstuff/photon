$(() => {

  const navList = $('#navContent');

  $('#content > section > article > header').each((i, element) => {
    const title = element.textContent;
    const name = i + '__' + title.replace(' ', '_');
    $(element).attr('id', name);

    const link$ = $('<li><a href="#' + name + '">' + title + '</a></li>')

    navList.append(link$);
  });

});
